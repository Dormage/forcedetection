
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;

public class Plate extends GridPane {
	public Sensor[][] sensors;
	int num_sensors;

	public Plate(int num_sensors) {
		this.num_sensors = num_sensors;
		sensors = new Sensor[num_sensors / 2][num_sensors / 2];
		this.setVgap(num_sensors / 2);
		this.setHgap(num_sensors / 2);
		this.setPadding(new Insets(20, 20, 20, 20));
		for (int i = 0; i < num_sensors / 2; i++) {
			for (int j = 0; j < num_sensors / 2; j++) {
				Sensor s = new Sensor(i + j);
				sensors[i][j] = s;
				s.getStyleClass().add("sensor");
				this.add(s, i, j);
			}
		}
	}

	public void update() {
		for (int i = 0; i < num_sensors / 2; i++) {
			for (int j = 0; j < num_sensors / 2; j++) {
				sensors[i][j].applyReadings();
			}
		}
	}
}
