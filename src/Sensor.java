import java.text.DecimalFormat;

import javafx.animation.FillTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Sensor extends Rectangle {
	int id;
	public double measurement;
	public double upper_bound;
	public double lower_bound;

	public Sensor(int id) {
		super(100, 100);
		this.setDisable(true);
		this.measurement = 0;
		this.id = id;
		//this.setPrefSize(100, 100);
		//this.setPadding(new Insets(25, 25, 25, 25));
	}

	public void applyReadings() {
		double previous = measurement;
		this.measurement = Math.random();
		DecimalFormat df = new DecimalFormat("#.##");
		//this.setText("" + df.format(this.measurement));
		DropShadow shadow = new DropShadow();
		shadow.setColor(getColor(measurement));
		shadow.setRadius(1000);
		//shadow.setSpread(50);

		Color c = getColor(this.measurement);
		//this.getShape().setFill(getColor(this.measurement));

		Timeline shadowAnimation = new Timeline(
		            new KeyFrame(Duration.ZERO, new KeyValue(shadow.colorProperty(),getColor(previous))),
		            new KeyFrame(Duration.millis(Main.frequncy), new KeyValue(shadow.colorProperty(), getColor(measurement))));

		this.setEffect(shadow);
		shadowAnimation.play();
		FillTransition ft = new FillTransition(Duration.millis(Main.frequncy), this, getColor(previous),
				getColor(measurement));
		ft.setCycleCount(1);
		ft.setAutoReverse(true);
		ft.play();
		this.setEffect(shadow);
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getMeasurement() {
		return measurement;
	}

	public void setMeasurement(int measurement) {
		this.measurement = measurement;
	}

	public Color getColor(double power) {
		power = 1 - power;
		double H = power * 0.4 * 360; // Hue (note 0.4 = Green)
		double S = 0.9; // Saturation
		double B = 0.9; // Brightness
		return Color.hsb(H, S, B, 1);
	}
}
